# Copyright (c) 2022-2023, NVIDIA CORPORATION & AFFILIATES. All rights reserved.
#
# See LICENSE for license information.

import requests
import sys
import os
import logging
import getpass


# Disable warning for unverified HTTPS requests
requests.packages.urllib3.disable_warnings()

# Set default vendor to "default"
vendor = "default"

# Configure logging
log_file = "oemSN-registration.log"
logging.basicConfig(filename=log_file, level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s")

# This function handles the Redfish API requests and returns the response object or None in case of an error
def handle_redfish_api(url, auth, verify=True):
    try:
        response = requests.get(url, auth=auth, verify=verify)
        response.raise_for_status()  # Raise an exception for 4xx or 5xx status codes
        logging.info(f"Request successful - URL: {url}")
    except requests.exceptions.RequestException as request_exception:
        logging.error(f"An error occurred: {request_exception}")
        return None
    else:
        return response

# This function returns the Traceability String for a given vendor and Redfish JSON data
def get_vendor_values(vendor, data):
    if vendor == "Lenovo":
        serial_number = data['SerialNumber']
        part_number = data['Model']
        TraceabilityString = "8S" + part_number + serial_number
        return TraceabilityString
    elif vendor == "Dell":
        serial_number = data['SerialNumber']
        part_number = data['PartNumber']
        part_number = part_number[:6]
        coo = serial_number[:2]
        actual_serial = serial_number[2:]
        TraceabilityString = coo + part_number + actual_serial
        return TraceabilityString
    elif vendor == "Fujitsu":
        return "not implemented"
    else:
        return "Invalid Vendor"

# This function retrieves the vendor name from a Redfish API response JSON data
def get_oem(ip, auth, verify):
    url = f'https://{ip}/redfish/v1'
    response = handle_redfish_api(url, auth, verify)
    if response is not None:
        data = response.json()
        vendor = data.get('Vendor')
        if vendor:
            logging.info(f"Vendor retrieved - IP: {ip}, Vendor: {vendor}")
            return vendor
    logging.error(f"Vendor retrieval failed - IP: {ip}")
    return "null"

# This function retrieves the DPU URL from a Redfish API response JSON data
def get_oem_dpu_url(ip, auth, verify):
    url = f'https://{ip}/redfish/v1/Systems'
    response = handle_redfish_api(url, auth, verify)
    if response is not None:
        data = response.json()
        for member in data.get('Members', []):
            if 'DPU' in member.get('@odata.id', ''):
                dpu_url = member['@odata.id']
                logging.info(f"DPU URL retrieved - IP: {ip}, DPU URL: {dpu_url}")
                return dpu_url
    logging.error(f"DPU URL retrieval failed - IP: {ip}")
    return "null"

# This function processes the given IP address by retrieving the vendor name, DPU URL, and Traceability String
def process_one_ip(ip, auth, verify):
    vendor = get_oem(ip, auth, verify)
    if vendor == "null":
        return
    dpu_url = get_oem_dpu_url(ip, auth, verify)
    if dpu_url == "null":
        return
    url = f'https://{ip}{dpu_url}'
    response = handle_redfish_api(url, auth, verify)
    if response is not None:
        data = response.json()
        traceability_string = get_vendor_values(vendor, data)
        logging.info(f"Traceability String generated - IP: {ip}, Traceability String: {traceability_string}")
        print(traceability_string)
    else:
        logging.error(f"Traceability String generation failed - IP: {ip}")

# This is the main function that reads in a file containing IP addresses and processes each IP for the Redfish API.
def main():
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print("Usage:" + sys.argv[0] + " <file containing IP address> [verify flag]")
        sys.exit(1)

    filename = sys.argv[1]

    # Determine verify flag based on command-line argument, or use default (True) if not provided
    verify = True
    if len(sys.argv) == 3:
        verify_flag = sys.argv[2]
        verify = True if verify_flag.lower() == 'true' else False


    # Set up authentication credentials
    username = input("Enter the username: ")
    password = getpass.getpass("Enter the password: ")
    auth = (username, password)

    # Open the file
    with open(filename, "r") as file:
        # Read the file line by line
        for line in file:
            # Remove any leading or trailing white space from the line
            ip_address = line.strip()
            logging.info(f"Processing IP: {ip_address}")
            process_one_ip(ip_address, auth, verify)

if __name__ == "__main__":
    main()

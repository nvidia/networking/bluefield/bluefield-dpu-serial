### Background
This script is the helper tool that helps Activate Your NVIDIA BlueField Enterprise Software Subscription for VMware vSphere 8 on NVIDIA BlueField DPU.
https://www.nvidia.com/en-us/networking/data-processing-unit/software-subscription-vsphere8/

This is a Python script that retrieves information from servers using the Redfish API. Here is a brief summary of what the script does:

- Sets up authentication credentials
```
The script will prompt you to enter the username and password to access the servers
```

## Code details
General code description:
- Defines a function handle_redfish_api that makes a GET request to a given URL with authentication and verifies the response status code. If there is an error, the function catches the exception and prints an error message before returning None. Otherwise, it returns the response.
- Defines a function get_vendor_values that takes a vendor and data as arguments and returns a traceability string based on the vendor's format for part and serial numbers.
- Defines a function get_oem that takes an IP address as an argument and retrieves the vendor from the Redfish API.
- Defines a function get_oem_dpu_url that takes an IP address as an argument and retrieves the DPU URL from the Redfish API.
- Defines a function process_one_ip that takes an IP address as an argument, retrieves the vendor and DPU URL using the get_oem and get_oem_dpu_url functions, respectively, and then retrieves data from the DPU URL and prints the traceability string using the get_vendor_values function.
- Defines a main function that reads IP addresses from a file and calls the process_one_ip function for each IP address.
- The format of the serverIP addresses (line separated) is as follows:
```
10.1.133.96 
10.1.133.95 
10.1.133.94
10.1.148.98
10.1.148.97 
10.1.148.96

```

## Prepare to run the Script

### Step 1 - DOWNLOAD:


```
Dowload the script from:
https://gitlab.com/nvidia/networking/bluefield/bluefield-dpu-serial/-/archive/main/bluefield-dpu-serial-main.zip
```

### Step 2 - UNZIP the downloaded file:

```
unzip bluefield-dpu-serial-main.zip
cd bluefield-dpu-serial

```
### Step 3:

```
pip3 install -r requirements.txt

```
### Step 4:

Edit the serverIP.txt file and add the IP addresses for the servers where the Bluefields are installed


## Step 5: Execute the Script

```

python3 get-bluefield-dpu-serial.py.py serverIPs.txt

```


Output of the script will be as follows if all goes well.

```
python3 get-bluefield-dpu-serial.py.py serverIPs.txt

IL0JNDCM74031282000J
IL0JNDCM74031282000E
IL0JNDCM740312820002
8SSN37B05881X1LM2980008
8SSN37B05881X1LM29C0006
8SSN37B05881X1LM2980006

```

Other possible outcomes - Bad username or password

```
if the username or password are incorrect:


python3 get-bluefield-dpu-serial.py.py serverIPs.txt

HTTP error occurred: 401 Client Error: Unauthorized for url: https://10.153.133.96/redfish/v1
HTTP error occurred: 401 Client Error: Unauthorized for url: https://10.153.133.95/redfish/v1
HTTP error occurred: 401 Client Error: Unauthorized for url: https://10.153.133.94/redfish/v1
HTTP error occurred: 401 Client Error: Unauthorized for url: https://10.153.148.98/redfish/v1/Systems
HTTP error occurred: 401 Client Error: Unauthorized for url: https://10.153.148.97/redfish/v1/Systems
HTTP error occurred: 401 Client Error: Unauthorized for url: https://10.153.148.96/redfish/v1/Systems

```

If server IP does not exists or down or not supported. In this case, IP address 10.10.10.10 was added to the serverIPs.txt

```
python3 get-bluefield-dpu-serial.py.py serverIPs.txt

IL0JNDCM74031282000J
IL0JNDCM74031282000E
IL0JNDCM740312820002
8SSN37B05881X1LM2980008
8SSN37B05881X1LM29C0006
Connection error occurred: HTTPSConnectionPool(host='10.10.10.10', port=443): Max retries exceeded with url: /redfish/v1 (Caused by NewConnectionError('<urllib3.connection.HTTPSConnection object at 0x7fe386e4b610>: Failed to establish a new connection: [Errno 113] No route to host'))
8SSN37B05881X1LM2980006

```




